using DataAccessLayer;
using BusinessContext;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AspNetCoreHero.ToastNotification;


namespace ContractWork
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // our code start
            // services.Configure<ApplicationSettings>(Configuration.GetSection("ApplicationSettings"));
            services.AddCors(options => {  // add cors
                options.AddDefaultPolicy(builder => {
                    builder.WithOrigins("http://localhost:5174/").AllowAnyHeader().AllowAnyMethod();

                });
            });
            //  services.AddControllers();
            //  services.AddControllers(x => x.AllowEmptyInputInBodyModelBinding = true);

            services.AddDbContext<ApplicationDbContext>(
                options =>
                {
                      options.UseSqlServer(Configuration.GetConnectionString("DbConnectSql"));
                   // options.UseNpgsql(Configuration.GetConnectionString("DbConnectSql"));

                }
                );

            //services.AddDbContext<ApplicationDbContext1>(
            //   options =>
            //   {
            //       //   options.UseSqlServer(Configuration.GetConnectionString("DbConnectSql_select"));
            //       options.UseNpgsql(Configuration.GetConnectionString("DbConnectSql_select"));
            //       //options.UseNpgsql(Configuration.GetConnectionString("DbConnectSql"));
            //   }
            //   );
            

            services.AddNotyf(config =>
            {
                config.DurationInSeconds = 5;
                config.IsDismissable = true;
                config.Position = NotyfPosition.TopRight;
            });
            //  services.AddDistributedMemoryCache();
            services.AddSession();


            // our code end
            //services.AddHttpContextAccessor();
            //services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
