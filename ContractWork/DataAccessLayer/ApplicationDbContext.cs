﻿
using DataAccessLayer.Models.General;
using DataAccessLayer.Models.Organization;
using DataAccessLayer.Models.Project;
using DataAccessLayer.Models.Testing;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
        public DbSet<Test> Test { get; set; }
        public DbSet<country_tbl> country_tbl { get; set; }
        public DbSet<job_type_tbl> job_type_tbl { get; set; }
        public DbSet<role_tbl> role_tbl { get; set; }
        public DbSet<state_tbl> state_tbl { get; set; }
        public DbSet<wrk_cat_tbl> wrk_cat_tbl { get; set; }

        public DbSet<brch_tbl> brch_tbl { get; set; }
        public DbSet<cntr_tbl> cntr_tbl { get; set; }
        public DbSet<emp_tbl> emp_tbl { get; set; }
        public DbSet<org_tbl> org_tbl { get; set; }
        public DbSet<user_tbl> user_tbl { get; set; }

        public DbSet<prj_meeting_attendance_tbl> prj_meeting_attendance_tbl { get; set; }
        public DbSet<prj_transaction_tbl> prj_transaction_tbl { get; set; }
        public DbSet<project_approval_tbl> project_approval_tbl { get; set; }
        public DbSet<project_history_tbl> project_history_tbl { get; set; }
        public DbSet<project_tbl> project_tbl { get; set; }
    }
}
