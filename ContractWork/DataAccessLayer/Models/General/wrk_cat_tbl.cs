﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models.General
{
    [Table("wrk_cat_tbl", Schema = "general")]
    public class wrk_cat_tbl
    {
        [Key]
        public int w_id { get; set; }
        [Required, Column(TypeName = "varchar(50)")]
        public string w_name { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string description { get; set; }
        [Required]
        public Boolean is_active { get; set; }
    }
}
