﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models.General
{
    [Table("role_tbl", Schema = "general")]
    public class role_tbl
    {
        [Key]
        public int r_id { get; set; }
        [Required, Column(TypeName = "varchar(50)")]
        public string r_name { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string description { get; set; }
        [Required]
        public Boolean is_active { get; set; }
    }
}
