﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models.General
{
    [Table("country_tbl", Schema = "general")]
    public class country_tbl
    {
        [Key]
        public int c_id { get; set; }
        [Required, Column(TypeName = "varchar(50)")]
        public string c_name { get; set; }
        [Required]
        public Boolean is_active { get; set; }
    }
}
