﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models.General
{
    [Table("state_tbl", Schema = "general")]
    public class state_tbl
    {
        [Key]
        public int s_id { get; set; }
        [Required, Column(TypeName = "varchar(50)")]
        public string s_name { get; set; }
        [Required,ForeignKey("country_tbl")]
        public int c_id { get; set; }
        [Required]
        public Boolean is_active { get; set; }
    }
}
