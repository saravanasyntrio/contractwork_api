﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models.Testing
{
    [Table("Test", Schema = "general")]
    public class Test
    {
        [Key]
        public int id { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string name { get; set; }
        [Column(TypeName = "varchar(200)")]
        public string address { get; set; }

        [Column(TypeName = "int")]
        public Boolean is_active { get; set; }


    }
}
