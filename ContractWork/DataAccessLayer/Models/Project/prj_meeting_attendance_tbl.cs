﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models.Project
{
    [Table("prj_meeting_attendance_tbl", Schema = "project")]
    public class prj_meeting_attendance_tbl
    {
        [Key]
        public int pm_id { get; set; }
        [ForeignKey("project_tbl")]
        public int prj_id { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string pm_attendese { get; set; }
       // [Column(TypeName = "timestamp without time zone")]
        public DateTime meeting_date { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string p_att_desc { get; set; }
    }
}
