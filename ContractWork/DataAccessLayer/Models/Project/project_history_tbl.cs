﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models.Project
{
    [Table("project_history_tbl", Schema = "project")]
    public class project_history_tbl
    {
        [Key]
        public int ph_id { get; set; }
        [ForeignKey("project_tbl")]
        public int prj_id { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string p_meeting_heading { get; set; }
     //   [Column(TypeName = "timestamp without time zone")]
        public DateTime meeting_date { get; set; }
        [Column(TypeName = "varchar(20)")]
        public string p_date { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string p_desc { get; set; }
    }
}
