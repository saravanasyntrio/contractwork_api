﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models.Project
{
    [Table("prj_transaction_tbl", Schema = "project")]
    public class prj_transaction_tbl
    {
        [Key]
        public int pt_id { get; set; }
        [Column(TypeName = "varchar(30)")]
        public string prj_total_cost { get; set; }
        [Column(TypeName = "varchar(30)")]
        public string prj_released_cost { get; set; }
        [Column(TypeName = "varchar(30)")]
        public string prj_discount { get; set; }
        [ForeignKey("emp_tbl")]
        public int prj_released_by { get; set; }
        [ForeignKey("cntr_tbl")]
        public int prj_get_whom { get; set; }
      //  [Column(TypeName = "timestamp without time zone")]
        public DateTime prj_date { get; set; }
        [Column(TypeName = "varchar(30)")]
        public string transation_mode { get; set; }
        [Column(TypeName = "varchar(30)")]
        public string prj_module { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string prj_desc { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string prj_status { get; set; }
    }
}
