﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models.Project
{
    [Table("project_tbl", Schema = "project")]
    public class project_tbl
    {
        [Key]
        public int prj_id { get; set; }
        [Required, Column(TypeName = "varchar(100)")]
        public string prj_name { get; set; }
        [ForeignKey("wrk_cat_tbl")]
        public int prj_cat { get; set; }
        [ForeignKey("job_type_tbl")]
        public int prj_type { get; set; }
        [Required, Column(TypeName = "varchar(20)")]
        public string prj_duration { get; set; }
        [Required, Column(TypeName = "varchar(30)")]
        public string prj_total_cost { get; set; }
      //  [Column(TypeName = "timestamp without time zone")]
        public DateTime prj_strt_date { get; set; }
     //   [Column(TypeName = "timestamp without time zone")]
        public DateTime prj_end_date { get; set; }
      //  [Column(TypeName = "timestamp without time zone")]
        public DateTime prj_expec_comp_date { get; set; }
      //  [Column(TypeName = "timestamp without time zone")]
        public DateTime prj_added_date { get; set; }
        [ForeignKey("emp_tbl")]
        public int prj_added_by { get; set; }
        [ Column(TypeName = "nvarchar(max)")]
        public string prj_added_desc { get; set; }
        [Required]
        public Boolean is_active { get; set; }
    }
}
