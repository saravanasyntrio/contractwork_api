﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models.Project
{
    [Table("project_approval_tbl", Schema = "project")]
    public class project_approval_tbl
    {
        [Key]
        public int pa_id { get; set; }
        [ForeignKey("project_tbl")]
        public int prj_id { get; set; }
      //  [Column(TypeName = "timestamp without time zone")]
        public DateTime approval_date { get; set; }
        [ForeignKey("emp_tbl")]
        public int approved_by { get; set; }
        [ForeignKey("cntr_tbl")]
        public int approved_client { get; set; }
        [Column(TypeName = "varchar(30)")]
        public string app_prj_duration { get; set; }
       // [Column(TypeName = "timestamp without time zone")]
        public DateTime app_strt_date { get; set; }
       // [Column(TypeName = "timestamp without time zone")]
        public DateTime app_end_date { get; set; }
       // [Column(TypeName = "timestamp without time zone")]
        public DateTime prj_expec_comp_date { get; set; }
      //  [Column(TypeName = "timestamp without time zone")]
        public DateTime prj_added_date { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string prj_app_desc { get; set; }
    }
}
