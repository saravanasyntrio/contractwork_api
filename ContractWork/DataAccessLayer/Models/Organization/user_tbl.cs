﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models.Organization
{
    [Table("user_tbl", Schema = "org")]
    public class user_tbl
    {
        [Key]
        public int u_id { get; set; }
        [Required]
        public int user_id { get; set; } //  user id from org,brch,emp and cnt tables
        [Required,Column(TypeName = "varchar(50)")]
        public string user_name { get; set; }
       
        [Required, Column(TypeName = "varchar(25)")]
        public string user_password { get; set; }
        [Required]
        public Boolean is_login { get; set; }
        
        [Column(TypeName = "varchar(100)")]
        public string token { get; set; }
        
       // [Column(TypeName = "timestamp without time zone")]
        public DateTime last_login { get; set; }
        

        [ForeignKey("role_tbl")]
        public int role_id { get; set; }
      //  [Column(TypeName = "timestamp without time zone")]
        public DateTime first_login { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string last_ip_add { get; set; }
        [Required]
        public Boolean is_active { get; set; }
    }
}
