﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models.Organization
{
    [Table("brch_tbl", Schema = "org")]
    public class brch_tbl
    {
        [Key]
        public int brch_id { get; set; }
        [Required, Column(TypeName = "varchar(200)")]
        public string brch_name { get; set; }
        [Column(TypeName = "varchar(200)")]
        public string brch_short_name { get; set; }
        
        [Required, ForeignKey("org_tbl")]
        public int org_parent { get; set; }
        [Required]
        public DateTime dba { get; set; }
        [Required, ForeignKey("country_tbl")]
        public int country_id { get; set; }
        [Required, ForeignKey("state_tbl")]
        public int state_id { get; set; }
        [Column(TypeName = "varchar(200)")]
        public string loc_address { get; set; }
        [Column(TypeName = "varchar(20)")]
        public string zip { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string license_no { get; set; }
        [Required, ForeignKey("wrk_cat_tbl")]
        public int org_cat_id { get; set; }
        [Column(TypeName = "varchar(25)")]
        public string lat_start { get; set; }
        [Column(TypeName = "varchar(25)")]
        public string lat_end { get; set; }
        [Column(TypeName = "varchar(25)")]
        public string long_start { get; set; }
        [Column(TypeName = "varchar(25)")]
        public string long_end { get; set; }
        [Required, Column(TypeName = "varchar(50)")]
        public string email1 { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string email2 { get; set; }
        [Required, Column(TypeName = "varchar(20)")]
        public string phone1 { get; set; }
        [Column(TypeName = "varchar(20)")]
        public string phone2 { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string website { get; set; }
        [Column(TypeName = "varchar(250)")]
        public string cmp_img { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string cmp_logo { get; set; }
       // [Column(TypeName = "timestamp without time zone")]
        public DateTime created_date { get; set; }
        public int approved_by { get; set; }   // From employee_tbl(admin)
        [Column(TypeName = "varchar(20)")]
        public string off_day { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string office_start { get; set; }  // office start time daily
        [Column(TypeName = "varchar(50)")]
        public string office_end { get; set; }  // office end time daily
        [Column(TypeName = "varchar(500)")]
        public string org_desc { get; set; }
        [Required, ForeignKey("emp_tbl")]
        public int emp_id { get; set; }
        [Required]
        public Boolean is_active { get; set; }
    }
}
