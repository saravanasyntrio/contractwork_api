﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Models.Organization
{
    [Table("cntr_tbl", Schema = "org")]
    public class cntr_tbl
    {
        [Key]
        public int con_id { get; set; }
        [Required, Column(TypeName = "varchar(200)")]
        public string first_name { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string last_name { get; set; }
       // [Column(TypeName = "timestamp without time zone")]
        public DateTime join_date { get; set; }
       // [Column(TypeName = "timestamp without time zone")]
        public DateTime approved_date { get; set; }
        [Required, ForeignKey("brch_tbl")]
        public int approved_by { get; set; }
        [Column(TypeName = "varchar(45)")]
        public string experience { get; set; }
        [ForeignKey("country_tbl")]
        public int country_id { get; set; }
        [ForeignKey("state_tbl")]
        public int state_id { get; set; }
        [Column(TypeName = "varchar(200)")]
        public string loc_address { get; set; }
        [Column(TypeName = "varchar(20)")]
        public string zip { get; set; }
        [Column(TypeName = "varchar(25)")]
        public string con_id_no { get; set; }
        [Required, Column(TypeName = "varchar(20)")]
        public string phone1 { get; set; }
        [Column(TypeName = "varchar(20)")]
        public string phone2 { get; set; }
        [Required, Column(TypeName = "varchar(50)")]
        public string email1 { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string email2 { get; set; }
        [Column(TypeName = "varchar(250)")]
        public string con_img { get; set; }
       // [Column(TypeName = "timestamp without time zone")]
        public DateTime created_date { get; set; }
        [Column(TypeName = "varchar(20)")]
        public string off_day { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string wrk_start { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string wrk_end { get; set; }

        [ForeignKey("role_tbl")]
        public int role_id { get; set; }
        [ForeignKey("brch_tbl")]
        public int branch_id { get; set; }

        [ForeignKey("wrk_cat_tbl")]
        public int designation { get; set; }
        [Column(TypeName = "varchar(10)")]
        public string gender { get; set; }
       // [Column(TypeName = "timestamp without time zone")]
        public DateTime dob { get; set; }
        [Column(TypeName = "varchar(25)")]
        public string highest_degree { get; set; }
        [Column(TypeName = "varchar(250)")]
        public string description { get; set; }
        [Required]
        public Boolean is_active { get; set; }
    }
}
