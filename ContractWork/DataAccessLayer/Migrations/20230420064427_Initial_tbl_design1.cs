﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccessLayer.Migrations
{
    public partial class Initial_tbl_design1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "org");

            migrationBuilder.EnsureSchema(
                name: "project");

            migrationBuilder.CreateTable(
                name: "country_tbl",
                schema: "general",
                columns: table => new
                {
                    c_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    c_name = table.Column<string>(type: "varchar(50)", nullable: false),
                    is_active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_country_tbl", x => x.c_id);
                });

            migrationBuilder.CreateTable(
                name: "job_type_tbl",
                schema: "general",
                columns: table => new
                {
                    j_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    j_type = table.Column<string>(type: "varchar(50)", nullable: false),
                    description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    is_active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_job_type_tbl", x => x.j_id);
                });

            migrationBuilder.CreateTable(
                name: "role_tbl",
                schema: "general",
                columns: table => new
                {
                    r_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    r_name = table.Column<string>(type: "varchar(50)", nullable: false),
                    description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    is_active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_role_tbl", x => x.r_id);
                });

            migrationBuilder.CreateTable(
                name: "state_tbl",
                schema: "general",
                columns: table => new
                {
                    s_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    s_name = table.Column<string>(type: "varchar(50)", nullable: false),
                    c_id = table.Column<int>(nullable: false),
                    is_active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_state_tbl", x => x.s_id);
                });

            migrationBuilder.CreateTable(
                name: "wrk_cat_tbl",
                schema: "general",
                columns: table => new
                {
                    w_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    w_name = table.Column<string>(type: "varchar(50)", nullable: false),
                    description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    is_active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_wrk_cat_tbl", x => x.w_id);
                });

            migrationBuilder.CreateTable(
                name: "brch_tbl",
                schema: "org",
                columns: table => new
                {
                    brch_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    brch_name = table.Column<string>(type: "varchar(200)", nullable: false),
                    brch_short_name = table.Column<string>(type: "varchar(200)", nullable: true),
                    org_parent = table.Column<int>(nullable: false),
                    dba = table.Column<DateTime>(nullable: false),
                    country_id = table.Column<int>(nullable: false),
                    state_id = table.Column<int>(nullable: false),
                    loc_address = table.Column<string>(type: "varchar(200)", nullable: true),
                    zip = table.Column<string>(type: "varchar(20)", nullable: true),
                    license_no = table.Column<string>(type: "varchar(100)", nullable: true),
                    org_cat_id = table.Column<int>(nullable: false),
                    lat_start = table.Column<string>(type: "varchar(25)", nullable: true),
                    lat_end = table.Column<string>(type: "varchar(25)", nullable: true),
                    long_start = table.Column<string>(type: "varchar(25)", nullable: true),
                    long_end = table.Column<string>(type: "varchar(25)", nullable: true),
                    email1 = table.Column<string>(type: "varchar(50)", nullable: false),
                    email2 = table.Column<string>(type: "varchar(50)", nullable: true),
                    phone1 = table.Column<string>(type: "varchar(20)", nullable: false),
                    phone2 = table.Column<string>(type: "varchar(20)", nullable: true),
                    website = table.Column<string>(type: "varchar(100)", nullable: true),
                    cmp_img = table.Column<string>(type: "varchar(250)", nullable: true),
                    cmp_logo = table.Column<string>(type: "varchar(100)", nullable: true),
                    created_date = table.Column<DateTime>(nullable: false),
                    approved_by = table.Column<int>(nullable: false),
                    off_day = table.Column<string>(type: "varchar(20)", nullable: true),
                    office_start = table.Column<string>(type: "varchar(50)", nullable: true),
                    office_end = table.Column<string>(type: "varchar(50)", nullable: true),
                    org_desc = table.Column<string>(type: "varchar(500)", nullable: true),
                    emp_id = table.Column<int>(nullable: false),
                    is_active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_brch_tbl", x => x.brch_id);
                });

            migrationBuilder.CreateTable(
                name: "cntr_tbl",
                schema: "org",
                columns: table => new
                {
                    con_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    first_name = table.Column<string>(type: "varchar(200)", nullable: false),
                    last_name = table.Column<string>(type: "varchar(50)", nullable: true),
                    join_date = table.Column<DateTime>(nullable: false),
                    approved_date = table.Column<DateTime>(nullable: false),
                    approved_by = table.Column<int>(nullable: false),
                    experience = table.Column<string>(type: "varchar(45)", nullable: true),
                    country_id = table.Column<int>(nullable: false),
                    state_id = table.Column<int>(nullable: false),
                    loc_address = table.Column<string>(type: "varchar(200)", nullable: true),
                    zip = table.Column<string>(type: "varchar(20)", nullable: true),
                    con_id_no = table.Column<string>(type: "varchar(25)", nullable: true),
                    phone1 = table.Column<string>(type: "varchar(20)", nullable: false),
                    phone2 = table.Column<string>(type: "varchar(20)", nullable: true),
                    email1 = table.Column<string>(type: "varchar(50)", nullable: false),
                    email2 = table.Column<string>(type: "varchar(50)", nullable: true),
                    con_img = table.Column<string>(type: "varchar(250)", nullable: true),
                    created_date = table.Column<DateTime>(nullable: false),
                    off_day = table.Column<string>(type: "varchar(20)", nullable: true),
                    wrk_start = table.Column<string>(type: "varchar(50)", nullable: true),
                    wrk_end = table.Column<string>(type: "varchar(50)", nullable: true),
                    role_id = table.Column<int>(nullable: false),
                    branch_id = table.Column<int>(nullable: false),
                    designation = table.Column<int>(nullable: false),
                    gender = table.Column<string>(type: "varchar(10)", nullable: true),
                    dob = table.Column<DateTime>(nullable: false),
                    highest_degree = table.Column<string>(type: "varchar(25)", nullable: true),
                    description = table.Column<string>(type: "varchar(250)", nullable: true),
                    is_active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cntr_tbl", x => x.con_id);
                });

            migrationBuilder.CreateTable(
                name: "emp_tbl",
                schema: "org",
                columns: table => new
                {
                    emp_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    first_name = table.Column<string>(type: "varchar(200)", nullable: false),
                    last_name = table.Column<string>(type: "varchar(50)", nullable: true),
                    join_date = table.Column<DateTime>(nullable: false),
                    approved_date = table.Column<DateTime>(nullable: false),
                    approved_by = table.Column<int>(nullable: false),
                    experience = table.Column<string>(type: "varchar(45)", nullable: true),
                    country_id = table.Column<int>(nullable: false),
                    state_id = table.Column<int>(nullable: false),
                    loc_address = table.Column<string>(type: "varchar(200)", nullable: true),
                    zip = table.Column<string>(type: "varchar(20)", nullable: true),
                    off_emp_id = table.Column<string>(type: "varchar(25)", nullable: true),
                    phone1 = table.Column<string>(type: "varchar(20)", nullable: false),
                    phone2 = table.Column<string>(type: "varchar(20)", nullable: true),
                    email1 = table.Column<string>(type: "varchar(50)", nullable: false),
                    email2 = table.Column<string>(type: "varchar(50)", nullable: true),
                    emp_img = table.Column<string>(type: "varchar(250)", nullable: true),
                    created_date = table.Column<DateTime>(nullable: false),
                    off_day = table.Column<string>(type: "varchar(20)", nullable: true),
                    wrk_start = table.Column<string>(type: "varchar(50)", nullable: true),
                    wrk_end = table.Column<string>(type: "varchar(50)", nullable: true),
                    desc = table.Column<string>(type: "varchar(250)", nullable: true),
                    role_id = table.Column<int>(nullable: false),
                    branch_id = table.Column<int>(nullable: false),
                    salary_range = table.Column<string>(type: "varchar(25)", nullable: true),
                    salary_type = table.Column<string>(type: "varchar(25)", nullable: true),
                    designation = table.Column<int>(nullable: false),
                    gender = table.Column<string>(type: "varchar(10)", nullable: true),
                    dob = table.Column<DateTime>(nullable: false),
                    highest_degree = table.Column<string>(type: "varchar(25)", nullable: true),
                    description = table.Column<string>(type: "varchar(250)", nullable: true),
                    Job_type = table.Column<int>(nullable: false),
                    is_active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_emp_tbl", x => x.emp_id);
                });

            migrationBuilder.CreateTable(
                name: "org_tbl",
                schema: "org",
                columns: table => new
                {
                    org_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    org_name = table.Column<string>(type: "varchar(200)", nullable: false),
                    short_name = table.Column<string>(type: "varchar(50)", nullable: true),
                    dba = table.Column<DateTime>(nullable: false),
                    country_id = table.Column<int>(nullable: false),
                    state_id = table.Column<int>(nullable: false),
                    loc_address = table.Column<string>(type: "varchar(200)", nullable: true),
                    zip = table.Column<string>(type: "varchar(20)", nullable: true),
                    license_no = table.Column<string>(type: "varchar(100)", nullable: true),
                    org_cat_id = table.Column<int>(nullable: false),
                    lat_start = table.Column<string>(type: "varchar(25)", nullable: true),
                    lat_end = table.Column<string>(type: "varchar(25)", nullable: true),
                    long_start = table.Column<string>(type: "varchar(25)", nullable: true),
                    long_end = table.Column<string>(type: "varchar(25)", nullable: true),
                    email1 = table.Column<string>(type: "varchar(50)", nullable: false),
                    email2 = table.Column<string>(type: "varchar(50)", nullable: true),
                    phone1 = table.Column<string>(type: "varchar(20)", nullable: false),
                    phone2 = table.Column<string>(type: "varchar(20)", nullable: true),
                    website = table.Column<string>(type: "varchar(100)", nullable: true),
                    org_img = table.Column<string>(type: "varchar(250)", nullable: true),
                    org_logo = table.Column<string>(type: "varchar(100)", nullable: true),
                    created_date = table.Column<DateTime>(nullable: false),
                    approved_by = table.Column<int>(nullable: false),
                    branch_avail = table.Column<bool>(nullable: false),
                    off_day = table.Column<string>(type: "varchar(20)", nullable: true),
                    office_start = table.Column<string>(type: "varchar(50)", nullable: true),
                    office_end = table.Column<string>(type: "varchar(50)", nullable: true),
                    org_desc = table.Column<string>(type: "varchar(500)", nullable: true),
                    emp_id = table.Column<int>(nullable: false),
                    is_active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_org_tbl", x => x.org_id);
                });

            migrationBuilder.CreateTable(
                name: "user_tbl",
                schema: "org",
                columns: table => new
                {
                    u_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    user_id = table.Column<int>(nullable: false),
                    user_name = table.Column<string>(type: "varchar(50)", nullable: false),
                    user_password = table.Column<string>(type: "varchar(25)", nullable: false),
                    is_login = table.Column<bool>(nullable: false),
                    token = table.Column<string>(type: "varchar(100)", nullable: true),
                    last_login = table.Column<DateTime>(nullable: false),
                    role_id = table.Column<int>(nullable: false),
                    first_login = table.Column<DateTime>(nullable: false),
                    last_ip_add = table.Column<string>(type: "varchar(100)", nullable: true),
                    is_active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_tbl", x => x.u_id);
                });

            migrationBuilder.CreateTable(
                name: "prj_meeting_attendance_tbl",
                schema: "project",
                columns: table => new
                {
                    pm_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    prj_id = table.Column<int>(nullable: false),
                    pm_attendese = table.Column<string>(type: "varchar(50)", nullable: true),
                    meeting_date = table.Column<DateTime>(nullable: false),
                    p_att_desc = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_prj_meeting_attendance_tbl", x => x.pm_id);
                });

            migrationBuilder.CreateTable(
                name: "prj_transaction_tbl",
                schema: "project",
                columns: table => new
                {
                    pt_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    prj_total_cost = table.Column<string>(type: "varchar(30)", nullable: true),
                    prj_released_cost = table.Column<string>(type: "varchar(30)", nullable: true),
                    prj_discount = table.Column<string>(type: "varchar(30)", nullable: true),
                    prj_released_by = table.Column<int>(nullable: false),
                    prj_get_whom = table.Column<int>(nullable: false),
                    prj_date = table.Column<DateTime>(nullable: false),
                    transation_mode = table.Column<string>(type: "varchar(30)", nullable: true),
                    prj_module = table.Column<string>(type: "varchar(30)", nullable: true),
                    prj_desc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    prj_status = table.Column<string>(type: "varchar(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_prj_transaction_tbl", x => x.pt_id);
                });

            migrationBuilder.CreateTable(
                name: "project_approval_tbl",
                schema: "project",
                columns: table => new
                {
                    pa_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    prj_id = table.Column<int>(nullable: false),
                    approval_date = table.Column<DateTime>(nullable: false),
                    approved_by = table.Column<int>(nullable: false),
                    approved_client = table.Column<int>(nullable: false),
                    app_prj_duration = table.Column<string>(type: "varchar(30)", nullable: true),
                    app_strt_date = table.Column<DateTime>(nullable: false),
                    app_end_date = table.Column<DateTime>(nullable: false),
                    prj_expec_comp_date = table.Column<DateTime>(nullable: false),
                    prj_added_date = table.Column<DateTime>(nullable: false),
                    prj_app_desc = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_project_approval_tbl", x => x.pa_id);
                });

            migrationBuilder.CreateTable(
                name: "project_history_tbl",
                schema: "project",
                columns: table => new
                {
                    ph_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    prj_id = table.Column<int>(nullable: false),
                    p_meeting_heading = table.Column<string>(type: "varchar(50)", nullable: true),
                    meeting_date = table.Column<DateTime>(nullable: false),
                    p_date = table.Column<string>(type: "varchar(20)", nullable: true),
                    p_desc = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_project_history_tbl", x => x.ph_id);
                });

            migrationBuilder.CreateTable(
                name: "project_tbl",
                schema: "project",
                columns: table => new
                {
                    prj_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    prj_name = table.Column<string>(type: "varchar(100)", nullable: false),
                    prj_cat = table.Column<int>(nullable: false),
                    prj_type = table.Column<int>(nullable: false),
                    prj_duration = table.Column<string>(type: "varchar(20)", nullable: false),
                    prj_total_cost = table.Column<string>(type: "varchar(30)", nullable: false),
                    prj_strt_date = table.Column<DateTime>(nullable: false),
                    prj_end_date = table.Column<DateTime>(nullable: false),
                    prj_expec_comp_date = table.Column<DateTime>(nullable: false),
                    prj_added_date = table.Column<DateTime>(nullable: false),
                    prj_added_by = table.Column<int>(nullable: false),
                    prj_added_desc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    is_active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_project_tbl", x => x.prj_id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "country_tbl",
                schema: "general");

            migrationBuilder.DropTable(
                name: "job_type_tbl",
                schema: "general");

            migrationBuilder.DropTable(
                name: "role_tbl",
                schema: "general");

            migrationBuilder.DropTable(
                name: "state_tbl",
                schema: "general");

            migrationBuilder.DropTable(
                name: "wrk_cat_tbl",
                schema: "general");

            migrationBuilder.DropTable(
                name: "brch_tbl",
                schema: "org");

            migrationBuilder.DropTable(
                name: "cntr_tbl",
                schema: "org");

            migrationBuilder.DropTable(
                name: "emp_tbl",
                schema: "org");

            migrationBuilder.DropTable(
                name: "org_tbl",
                schema: "org");

            migrationBuilder.DropTable(
                name: "user_tbl",
                schema: "org");

            migrationBuilder.DropTable(
                name: "prj_meeting_attendance_tbl",
                schema: "project");

            migrationBuilder.DropTable(
                name: "prj_transaction_tbl",
                schema: "project");

            migrationBuilder.DropTable(
                name: "project_approval_tbl",
                schema: "project");

            migrationBuilder.DropTable(
                name: "project_history_tbl",
                schema: "project");

            migrationBuilder.DropTable(
                name: "project_tbl",
                schema: "project");
        }
    }
}
